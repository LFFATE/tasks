export const environment = {
  production: true,
  client: {
    baseUrl: 'http://tasks.fun-work.ru',
  },
  mockBackend: false,
  updateInterval: 60000
};
