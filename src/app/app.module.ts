import { BrowserModule }      from '@angular/platform-browser';
import { NgModule }           from '@angular/core';
import { FormsModule }        from '@angular/forms';
import { HttpClientModule,
         HTTP_INTERCEPTORS }  from '@angular/common/http';

import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';

import { StoreModule }          from '@ngrx/store';
import { EffectsModule }        from '@ngrx/effects';
import { StoreDevtoolsModule }  from "@ngrx/store-devtools";

import { ActionReducerMap, ActionReducer, MetaReducer } from '@ngrx/store';
import { localStorageSync } from 'ngrx-store-localstorage';

import { environment } from '../environments/environment';

import { InMemDataMockService }   from './_services/_fake/mock.data.service';
import { MessageService }  from './_services/index';

import { AppComponent }  from './app.component';
import { routing }       from './app.routing';

import { AuthGuard } from './_guards/index';
import { ErrorInterceptor } from './_interceptors';
import { TokenInterceptor } from './_interceptors';

import { AuthReducer } from './_ngrx/reducers/auth';
import { TasksReducer } from './_ngrx/reducers/tasks';
import { AuthEffects } from './_ngrx/effects/auth';
import { TasksEffects } from './_ngrx/effects/tasks';
import { AuthState } from './_ngrx/states/auth';
import { State } from './_ngrx/states';

import { LoginComponent } from './login/index';
import { HomeComponent } from './home/index';
import { AuthComponent } from './auth/index';
import { TaskComponent } from './task/index';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Ng2DragDropModule } from 'ng2-drag-drop';

export function localStorageSyncReducer(reducer: ActionReducer<any>): ActionReducer<any> {
  return localStorageSync({
    keys: ['auth', 'tasks'],
    rehydrate: true
  })(reducer);
}

const metaReducers: Array<MetaReducer<any, any>> = [localStorageSyncReducer];

const reducers: ActionReducerMap<State> = {
  auth: AuthReducer,
  tasks: TasksReducer
};

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    AuthComponent,
    TaskComponent
  ],
  imports: [
    BrowserModule,

    // состояние приложения
    StoreModule.forRoot(
      reducers,
      { metaReducers }
    ),

    FormsModule,
    HttpClientModule,
    routing,

    // бутстрап
    NgbModule.forRoot(),
    // Drag and Drop
    Ng2DragDropModule.forRoot(),

    EffectsModule.forRoot([
      AuthEffects,
      TasksEffects
    ]),

    // расширение для отладки redux
    StoreDevtoolsModule.instrument(),

    // фейковый бэк c перехватчиками запросов и ответов
    environment.mockBackend ? HttpClientInMemoryWebApiModule.forRoot(InMemDataMockService, { delay: 1500 }) : [],
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
    AuthGuard,
    MessageService,
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
