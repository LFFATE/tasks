import { Component, OnInit } from '@angular/core';
import { AuthState } from '../_ngrx/states/auth';
import { Store } from '@ngrx/store';
import * as Auth from '../_ngrx/actions/auth';
import * as AuthReducers from '../_ngrx/reducers/auth';
import { User } from '../_models';

@Component({
  moduleId: module.id,
  selector: 'auth-component',
  templateUrl: 'auth.component.html',
  styleUrls: [ 'auth.component.scss' ]
})

export class AuthComponent implements OnInit {
  user: User;

  constructor
  (
    private store: Store<AuthState>,
  )
  {
    this.store
      .select(AuthReducers.getUser)
      .subscribe((user: User) => {
        this.user = user;
      });
  }

  ngOnInit() {

  }

  logout() {
    this.store.dispatch(new Auth.Logout());
  }
}
