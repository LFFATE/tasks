import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse,
  HttpErrorResponse
} from '@angular/common/http';

import { Store } from '@ngrx/store';
import { AuthState } from '../_ngrx/states/auth';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';

import * as AuthActions from '../_ngrx/actions/auth';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  constructor(
    private store: Store<AuthState>
  ) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    return next.handle(request).do((event: HttpEvent<any>) => {
      if (event instanceof HttpResponse) {
        console.log('response interceptor');
      }
    }, (err: any) => {
      if (err instanceof HttpErrorResponse) {
        switch (err.status) {
          case 401:
            console.log('401 error interceptor');
            break;
          case 404:
            console.log('404 error interceptor');
            break;
          case 405:
            console.log('405 error interceptor');
            this.store.dispatch(new AuthActions.LoginRedirect());
            break;
        }
      }
    });
  }
}
