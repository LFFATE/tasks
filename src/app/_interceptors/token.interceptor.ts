import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';

import { Observable } from 'rxjs/Observable';


import { AuthState } from '../_ngrx/states/auth';
import * as AuthReducers from '../_ngrx/reducers/auth';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(private store: Store<AuthState>,) {}
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    this.store
      .select(AuthReducers.getAuthToken)
      .subscribe((token: string) => {
        request = request.clone({
          setHeaders: {
            Authorization: `Bearer ${token}`
          }
        });
      });

    return next.handle(request);
  }
}
