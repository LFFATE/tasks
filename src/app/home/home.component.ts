import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import 'rxjs/add/observable/interval';
import { sortBy } from 'lodash';

import { environment } from '../../environments/environment';

import * as TasksReducers from '../_ngrx/reducers/tasks';
import * as TasksActions from '../_ngrx/actions/tasks';
import { TasksState } from '../_ngrx/states/tasks';
import { Task, Category, ViewMode, SortMode } from '../_models';

@Component({
  moduleId: module.id,
  templateUrl: 'home.component.html',
})

export class HomeComponent implements OnInit {
  viewModeModel: ViewMode;
  sortModeModel: ViewMode;
  taskList: Task[] = [];
  categoryList: Category[] = [];
  viewMode = ViewMode;
  loading: boolean = false;
  sortMode = SortMode;

  constructor(
    private store: Store<TasksState>,
  ) {
    this.store
      .select(TasksReducers.isLoading)
      .subscribe((loading: boolean) => {
        this.loading = loading;
      });

    this.store
      .select(TasksReducers.getCategoriesWithTasks)
      .subscribe((categories: Category[]) => {
        this.categoryList = categories;
      });

    this.store
      .select(TasksReducers.getViewMode)
      .subscribe((viewMode: ViewMode) => {
        this.viewModeModel = viewMode;
      });

    this.store
      .select(TasksReducers.getTasksOnly)
      .subscribe((tasks: Task[]) => {
        this.taskList = sortBy(tasks, [ this.sortModeModel ]);
      });
  }

  ngOnInit() {
    this.store.dispatch(new TasksActions.GetTaskList());

    setInterval(() => {
      this.store.dispatch(new TasksActions.GetTaskList());
    }, environment.updateInterval);
  }

  setViewMode(mode: ViewMode) {
    this.store.dispatch(new TasksActions.SetViewMode(mode));
  }

  setSortMode(mode: SortMode) {
    this.store.dispatch(new TasksActions.SetSortMode(mode));
  }

  onItemDrop(dragData: { task: Task, category: Category }, categoryId: number) {
    this.store.dispatch(new TasksActions.ChangeTaskCategory({ task: dragData.task, category: dragData.category, categoryId }));
  }
}
