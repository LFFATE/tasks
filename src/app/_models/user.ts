export class User {
  id: number;
  username: string;
  loggedIn?: boolean;
  password?: string;
}

export interface Authenticate {
  username: string;
  password: string;
}

export interface CurrentUser extends User {
  token: string;
}
