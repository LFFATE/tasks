import { schema } from 'normalizr';
import { User } from './user';

export class Task {
  id: number;
  title: string;
  description: string;
  user: User;
  categoryId?: number;
  category?: string;
  categoryCode?: string;
  priority: number;
  dateCreate: string;
  dateStart?: string;
  timePlan?: string;
  timeReal?: string;
}

export class Category {
  id: number;
  name: string;
  code: string;
  tasks?: Task[];
  taskIds?: number[];
}

export enum ViewMode {
  Table = "TABLE",
  FullTable = "FULL_TABLE",
  Board = "BOARD"
}

export enum SortMode {
  Id = "id",
  Status = "categoryId",
  Prior = "priority",
  Title = "title"
}

export const taskSchema = new schema.Entity('tasks');
export const categorySchema = new schema.Entity('categories', { tasks: [ taskSchema ] });

export const categoryListSchema = new schema.Array(categorySchema);
