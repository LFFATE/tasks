import { HttpClient } from '@angular/common/http';

import { environment } from '../../../environments/environment';

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Actions, Effect } from '@ngrx/effects';
import { AddData } from 'ngrx-normalizr';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/do';
import 'rxjs/add/observable/of';

import * as TasksActions from '../actions/tasks';
import { Action } from '../../_ngrx/actions/action';
import { Category, categorySchema} from '../../_models';
@Injectable()
export class TasksEffects {

  constructor(
    private http: HttpClient,
    private actions$: Actions
  ) { }

  @Effect()
  getTaskList$: Observable<Action> = this.actions$
    .ofType(TasksActions.GET_TASK_LIST)
    .switchMap((action: Action) => {

      return this.http
        .get(environment.client.baseUrl + '/api/tasks')
        .map((data: any) => {
          return new TasksActions.GetTaskListSuccess(data);
        })
    });

  @Effect()
  getTask$: Observable<Action> = this.actions$
    .ofType(TasksActions.GET_TASK)
    .switchMap((action: Action) => {

      return this.http
        .get(environment.client.baseUrl + '/api/tasks/' + action.payload)
        .map((data: any) => {
          return new TasksActions.GetTaskSuccess(data);
        })
    });

  @Effect()
  changeTaskCategory$: Observable<Action> = this.actions$
    .ofType(TasksActions.CHANGE_TASK_CATEGORY)
    .mergeMap((action: Action) => {

      return this.http
        .patch(environment.client.baseUrl + '/api/tasks/' + action.payload.taskId, { categoryId: action.payload.categoryId })
        .map(() => {
          return new TasksActions.ChangeTaskCategorySuccess(action.payload);
        })
        .catch((error) => {
          return Observable.of(new TasksActions.ChangeTaskCategoryError(error));
        })
    });
}
