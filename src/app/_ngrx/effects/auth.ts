import { HttpClient } from '@angular/common/http';

import { environment } from '../../../environments/environment';

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Actions, Effect } from '@ngrx/effects';
import { Router } from '@angular/router';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/do';
import 'rxjs/add/observable/of';

import * as AuthActions from '../actions/auth';
import { Action } from '../../_ngrx/actions/action';

@Injectable()
export class AuthEffects {

  constructor(
    private http: HttpClient,
    private router: Router,
    private actions$: Actions
  ) { }

  @Effect()
  Login$: Observable<Action> = this.actions$
    .ofType(AuthActions.LOGIN)
    .switchMap((action: Action) => {
      return this.http
        .post<string>(environment.client.baseUrl + '/api/authenticate', JSON.stringify(action.payload))
        .map((token: string) => {
          return new AuthActions.LoginSuccess(token);
        })
        .catch((err) => {
          return Observable.of(new AuthActions.LoginError(err));
        })
    });

  @Effect({ dispatch: false })
  loginSuccess$ = this.actions$
    .ofType(AuthActions.LOGIN_SUCCESS)
    .do(() => {
      return this.router.navigate(['/']);
    });

  @Effect({ dispatch: false })
  loginRedirect$ = this.actions$
    .ofType(AuthActions.LOGOUT, AuthActions.LOGIN_REDIRECT)
    .do(() => {
      return this.router.navigate(['/login']);
    });
}
