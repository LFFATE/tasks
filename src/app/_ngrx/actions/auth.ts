import { Action } from '@ngrx/store';
import { Authenticate } from '../../_models';

export const LOGIN  = '[Auth] Login';
export const LOGIN_SUCCESS  = '[Auth] LoginSuccess';
export const LOGIN_ERROR  = '[Auth] LoginError';
export const LOGIN_REDIRECT  = '[Auth] LoginRedirect';
export const LOGOUT  = '[Auth] Logout';

export class Login implements Action {
  readonly type = LOGIN;
  constructor(public payload: Authenticate) {}
}

export class LoginSuccess implements Action {
  readonly type = LOGIN_SUCCESS;
  constructor(public payload: any) {}
}

export class LoginError implements Action {
  readonly type = LOGIN_ERROR;
  constructor(public payload: any) {}
}

export class LoginRedirect implements Action {
  readonly type = LOGIN_REDIRECT;
}

export class Logout implements Action {
  readonly type = LOGOUT;
}

export type All
  = Login
  | LoginSuccess
  | LoginRedirect
  | LoginError
  | Logout;
