import { Action } from '@ngrx/store';
import { Task } from '../../_models';

export const GET_TASK  = '[Task] GetTask';
export const GET_TASK_SUCCESS  = '[Task] GetTaskSuccess';

export const GET_TASK_LIST  = '[Task] GetTaskList';
export const GET_TASK_LIST_SUCCESS  = '[Task] GetTaskListSuccess';

export const SET_VIEW_MODE  = '[Task] SetViewMode';
export const SET_SORT_MODE  = '[Task] SetSortMode';

export const CHANGE_TASK_CATEGORY  = '[Task] ChangeTaskCategory';
export const CHANGE_TASK_CATEGORY_SUCCESS  = '[Task] ChangeTaskCategorySuccess';
export const CHANGE_TASK_CATEGORY_ERROR  = '[Task] ChangeTaskCategoryError';

export const SET_CURRENT_TASK  = '[Task] SetCurrentTask';

export class GetTaskList implements Action {
  readonly type = GET_TASK_LIST;
}

export class GetTaskListSuccess implements Action {
  readonly type = GET_TASK_LIST_SUCCESS;
  constructor(public payload: any) {}
}

export class SetViewMode implements Action {
  readonly type = SET_VIEW_MODE;
  constructor(public payload: any) {}
}

export class SetSortMode implements Action {
  readonly type = SET_SORT_MODE;
  constructor(public payload: any) {}
}

export class ChangeTaskCategory implements Action {
  readonly type = CHANGE_TASK_CATEGORY;
  constructor(public payload: any) {}
}

export class ChangeTaskCategorySuccess implements Action {
  readonly type = CHANGE_TASK_CATEGORY_SUCCESS;
  constructor(public payload: any) {}
}

export class ChangeTaskCategoryError implements Action {
  readonly type = CHANGE_TASK_CATEGORY_ERROR;

  constructor(public payload: any) {
  }
}

export class SetCurrentTask implements Action {
  readonly type = SET_CURRENT_TASK;
  constructor(public payload: any) {}
}

export class GetTask implements Action {
  readonly type = GET_TASK;
  constructor(public payload: any) {}
}

export class GetTaskSuccess implements Action {
  readonly type = GET_TASK_SUCCESS;
  constructor(public payload: any) {}
}

export type All
  = GetTaskList
  | GetTask
  | GetTaskSuccess
  | SetViewMode
  | SetSortMode
  | GetTaskListSuccess
  | ChangeTaskCategory
  | ChangeTaskCategorySuccess
  | ChangeTaskCategoryError
  | SetCurrentTask;
