import { Action as A} from '@ngrx/store';

export interface Action extends A {
  payload: any;
}
