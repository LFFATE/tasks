import { Task, Category, ViewMode, SortMode } from '../../_models';

export interface TasksState {
  categories: Category[],
  tasks: Task[],
  viewMode: ViewMode,
  sortMode: SortMode,
  loading: boolean,
  error?: string,
  current?: number,
  task?: Task
}

export const tasksDefaultState = {
  categories: [],
  tasks: [],
  viewMode: ViewMode.Table,
  sortMode: SortMode.Prior,
  loading: false,
  error: '',
  current: 0
};
