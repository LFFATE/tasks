export interface AuthState {
  id: number;
  username: string;
  token: string;
  loggedIn: boolean;
  loading: boolean;
  error: string;
}

export const authDefaultState = {
  id: 0,
  username: 'Guest',
  token: '',
  loggedIn: false,
  loading: false,
  error: ''
};
