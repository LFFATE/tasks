import { AuthState } from './auth';
import { TasksState } from './tasks';

export interface State {
  auth: AuthState;
  tasks: TasksState;
}
