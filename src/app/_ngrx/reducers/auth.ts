import { createSelector, createFeatureSelector } from '@ngrx/store';

import * as AuthActions from '../actions/auth';
import { authDefaultState, AuthState } from '../states/auth';
import { State } from '../states';

export type Action = AuthActions.All;

export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGOUT = 'LOGOUT';

export function AuthReducer(state = authDefaultState, action: Action) {

  switch (action.type) {
    case AuthActions.LOGIN:

      return { ...state, loading: true, error: '' }

    case AuthActions.LOGIN_SUCCESS:

      return Object.assign({}, state, {
        id: action.payload.id,
        username: action.payload.username,
        token: action.payload.token,
        loggedIn: true,
        loading: false,
        error: ''
      });

    case AuthActions.LOGOUT:
      return { ...state, token: '', loggedIn: false, id: 0, username: 'Guest' };

    case AuthActions.LOGIN_ERROR:

      return Object.assign({}, state, {
        error: action.payload.error,
        loading: false
      });

    default:
      return state;
  }
}

export const selectAuthStore =  createFeatureSelector<AuthState>('auth');
export const getUser      = createSelector(selectAuthStore, (state: AuthState) => {
  return {
    id: state.id,
    username: state.username,
    loggedIn: state.loggedIn
  }
});
export const getLoggedIn  = createSelector(selectAuthStore, (state: AuthState) => state.loggedIn);
export const getUserName  = createSelector(selectAuthStore, (state: AuthState) => state.username);
export const getUserId    = createSelector(selectAuthStore, (state: AuthState) => state.id);
export const getAuthToken = createSelector(selectAuthStore, (state: AuthState) => state.token);

export const getError = createSelector(selectAuthStore, (state: AuthState) => state.error);
export const getLoading = createSelector(selectAuthStore, (state: AuthState) => state.loading);
