import { createSelector, createFeatureSelector } from '@ngrx/store';
import { normalize, denormalize } from 'normalizr';
import { toArray, includes, head, forEach, find } from 'lodash';

import * as TasksActions from '../actions/tasks';
import { tasksDefaultState, TasksState } from '../states/tasks';
import { State } from '../states';
import { categoryListSchema, Category, Task, /*taskSchema*/ } from '../../_models';
import {categorySchema} from '../../_models/task';

export type Action = TasksActions.All;

export const GET_TASK = 'GET_TASK';
export const GET_TASK_SUCCESS = 'GET_TASK_SUCCESS';
export const GET_TASK_LIST = 'GET_TASK_LIST';
export const GET_TASK_LIST_SUCCESS = 'GET_TASK_LIST_SUCCESS';
export const SET_SORT_MODE = 'SET_SORT_MODE';
export const SET_VIEW_MODE = 'SET_VIEW_MODE';
export const CHANGE_TASK_CATEGORY = 'CHANGE_TASK_CATEGORY';
export const CHANGE_TASK_CATEGORY_SUCCESS = 'CHANGE_TASK_CATEGORY_SUCCESS';
export const SET_CURRENT_TASK = 'SET_CURRENT_TASK';

export function TasksReducer(state = tasksDefaultState, action: Action) {

  switch (action.type) {
    case TasksActions.GET_TASK_LIST:

      return { ...state, loading: true, error: '' };

    case TasksActions.GET_TASK_LIST_SUCCESS:
      let normalizedData = normalize(action.payload, categoryListSchema);
      return { ...state,
        categories: normalizedData.entities.categories,
        tasks: normalizedData.entities.tasks,
        loading: false
      };

    case TasksActions.SET_VIEW_MODE:
      return { ...state, viewMode:  action.payload};

    case TasksActions.SET_SORT_MODE:
      return { ...state, sortMode:  action.payload};

    case TasksActions.SET_CURRENT_TASK:
      return { ...state, current: action.payload, task: null };

    case TasksActions.GET_TASK:
      return { ...state, loading: true };

    case TasksActions.GET_TASK_SUCCESS:
      return { ...state, task: action.payload, loading: false };

    case TasksActions.CHANGE_TASK_CATEGORY:
      return { ...state, loading: true };

    case TasksActions.CHANGE_TASK_CATEGORY_SUCCESS:
      let categoryFrom: Category = action.payload.category;
      let categoryToId: number = action.payload.categoryId;
      let task: Task = action.payload.task;

      let newTaskArr: number[] = state.categories[categoryToId].tasks;
      if (! includes(newTaskArr, task.id)) newTaskArr.push(task.id);

      return {
        ...state,
        loading: false,
        categories:
          {...state.categories,
            [categoryFrom.id]: {
              ...state.categories[categoryFrom.id],
              tasks: state.categories[categoryFrom.id].tasks.filter(e => e !== task.id)
            },
            [categoryToId]: {
              ...state.categories[categoryToId],
              tasks: newTaskArr
            }
          }
      };

    case TasksActions.CHANGE_TASK_CATEGORY_ERROR:
      return { ...state, loading: false };

    default:
      return state;
  }
}

export const selectTasksStore =  createFeatureSelector<TasksState>('tasks');

export const getTasksOnly    = createSelector(selectTasksStore, (state: TasksState) => {
  let taskList: Task[] = [];
  forEach(state.tasks, (task: Task) => {
    let parent = find(state.categories, (category: Category) => includes(category.tasks, task.id));
    task.categoryCode = parent.code;
    task.category = parent.name;
    task.categoryId = parent.id;
    taskList.push(task);
  });

  return taskList;
});

export const getCategories = createSelector(selectTasksStore, (state: TasksState) => toArray(state.categories));
export const getCurrentTask = createSelector(selectTasksStore, (state: TasksState) => state.task);
export const getCategoriesWithTasks = createSelector(selectTasksStore, (state: TasksState) => {
  let categoryIds = Object.keys(state.categories);
  let denormalizedData = denormalize(categoryIds, categoryListSchema, {categories: state.categories, tasks: state.tasks});

  return denormalizedData;
});

export const getError = createSelector(selectTasksStore, (state: TasksState) => state.error);
export const isLoading = createSelector(selectTasksStore, (state: TasksState) => state.loading);

export const getViewMode = createSelector(selectTasksStore, (state: TasksState) => state.viewMode);
export const getSortMode = createSelector(selectTasksStore, (state: TasksState) => state.sortMode);
