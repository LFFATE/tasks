import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login';
import { HomeComponent } from './home';
import { TaskComponent } from './task';
import { AuthGuard } from './_guards';

const appRoutes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: '', component: HomeComponent, canActivate: [ AuthGuard ] },
  { path: 'tasks/:id', component: TaskComponent, canActivate: [ AuthGuard ] },

  { path: '**', redirectTo: '' }
];

export const routing = RouterModule.forRoot(appRoutes);
