import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';

import { Authenticate } from '../_models';
import * as Auth from '../_ngrx/actions/auth';
import { AuthState } from '../_ngrx/states/auth';
import * as AuthReducers from '../_ngrx/reducers/auth';

@Component({
  moduleId: module.id,
  selector: 'login-component',
  templateUrl: 'login.component.html',
  styleUrls: ['login.component.scss']
})

export class LoginComponent implements OnInit {
  model: Authenticate = { username: '', password: '' };
  loading: boolean = false;
  error: string = '';

  constructor
  (
    private router: Router,
    private store: Store<AuthState>,
  )
  {
    this.store
      .select(AuthReducers.getError)
      .subscribe((error: string) => {
        this.error = error;
      });
    this.store
      .select(AuthReducers.getLoading)
      .subscribe((loading: boolean) => {
        this.loading = loading;
      });
  }

  ngOnInit() {

  }

  login() {
    this.loading = true;
    this.store.dispatch(new Auth.Login(this.model));
  }
}
