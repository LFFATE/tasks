import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'

import { Task } from '../_models';

@Injectable()
export class UserService {
  constructor(
    private http: Http,
  ) {
  }

  getTasks(): Observable<Task[]> {
    let headers = new Headers({ 'Authorization': 'Bearer ' });
    let options = new RequestOptions({ headers: headers });

    return this.http.get('/api/users', options)
      .map((response: Response) => response.json());
  }
}
