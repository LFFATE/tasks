import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { ParsedRequestUrl, RequestInfo, RequestInfoUtilities, ResponseOptions } from 'angular-in-memory-web-api/interfaces';
import { getStatusText, STATUS } from 'angular-in-memory-web-api/http-status-codes';
import { forEach, filter, find } from 'lodash';
import { User, Task } from '../../_models';

import * as tasks from './tasks.json';

const users = [
  { id: 1, username: 'test', password: 'test' },
  { id: 2, username: 'prot', password: 'test' },
  { id: 3, username: 'Powell', password: 'test' },
];

export class InMemDataMockService implements InMemoryDbService {

  get(reqInfo: RequestInfo) {
    const collectionName = reqInfo.collectionName;

    switch(collectionName) {
      case 'tasks':
        if (reqInfo.id) return this.getTask(reqInfo);
    }

    return undefined;
  }

  post(reqInfo: RequestInfo) {
    const collectionName = reqInfo.collectionName;

    switch(collectionName) {
      case 'authenticate':
        return this.authenticate(reqInfo);
    }

    return undefined;
  }

  patch(reqInfo: RequestInfo) {
    const collectionName = reqInfo.collectionName;

    switch(collectionName) {
      case 'tasks':
        return this.patchTask(reqInfo);
    }

    return undefined;
  }

  /**
   * эмуляция ответа сервера за запрос авторизации
   * @param {RequestInfo} reqInfo
   * @returns {Observable<any>}
   */
  private authenticate(reqInfo: RequestInfo) {

    let requestBody = reqInfo.utils.getJsonBody(reqInfo.req);
    let credentials = JSON.parse(requestBody);

    return reqInfo.utils.createResponse$(() => {
      console.log('HTTP override');
      let token:string = null;
      let response: any;

      const user = users.filter((user: User) => {
        return user.username === credentials.username && user.password === credentials.password;
      })[0];

      if (user) {
        response = {
          token: 'asdasdkjasd2134234',
          username: user.username,
          id: user.id
        };

        return new HttpResponse({
          body: response,
          status: STATUS.OK,
          statusText: getStatusText(STATUS.OK),
          url: reqInfo.url
        });
      } else {

        return new HttpErrorResponse({
          error: 'Ошибка авторизации',
          status: STATUS.UNAUTHORIZED,
          statusText: getStatusText(STATUS.UNAUTHORIZED),
          url: reqInfo.url
        });
      }
    });
  }

  /**
   * эмуляция ответа сервера на запрос изменения задачи
   * @param {RequestInfo} reqInfo
   * @returns {HttpResponse<any>}
   */
  private patchTask(reqInfo: RequestInfo)
  {
    return reqInfo.utils.createResponse$(() => {
      return new HttpResponse({
        status: STATUS.OK,
        statusText: getStatusText(STATUS.OK),
        url: reqInfo.url
      });
    });
  }

  /**
   * эмуляция ответа сервера на запрос задачи
   * @param {RequestInfo} reqInfo
   * @returns {HttpResponse<any>}
   */
  private getTask(reqInfo: RequestInfo)
  {
    let task: Task;

    forEach(tasks, (category) => {
      task = find(category.tasks, ['id', reqInfo.id]);
      if (task !== undefined) return false;
    });

    return reqInfo.utils.createResponse$(() => {
      if (task) {

        return new HttpResponse({
          body: task,
          status: STATUS.OK,
          statusText: getStatusText(STATUS.OK),
          url: reqInfo.url
        });
      } else {

        return new HttpErrorResponse({
          error: 'Задача не найдена',
          status: STATUS.NOT_FOUND,
          statusText: getStatusText(STATUS.NOT_FOUND),
          url: reqInfo.url
        });
      }
    });
  }

  createDb() {
    return { users, tasks };
  }
}
