import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { AuthState } from '../_ngrx/states/auth';
import * as AuthReducers from '../_ngrx/reducers/auth';
import * as AuthActions from '../_ngrx/actions/auth';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Rx';

import 'rxjs/add/operator/take';
import 'rxjs/add/operator/map';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor
  (
    private store: Store<AuthState>
  ) {

  }

  canActivate(): Observable<boolean> {
    return this.store
      .select(AuthReducers.getLoggedIn)
      .map(authed => {
        if (! authed) {
          this.store.dispatch(new AuthActions.LoginRedirect());
          return false;
        }

        return true;
      })
      .take(1);
  }
}
