import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { ActivatedRoute } from '@angular/router';

import 'rxjs/add/observable/interval';
import { sortBy } from 'lodash';

import * as TasksReducers from '../_ngrx/reducers/tasks';
import * as TasksActions from '../_ngrx/actions/tasks';
import { TasksState } from '../_ngrx/states/tasks';
import { Task, Category, ViewMode, SortMode } from '../_models';

@Component({
  moduleId: module.id,
  templateUrl: 'task.component.html',
})

export class TaskComponent implements OnInit {
  loading: boolean = false;
  task: Task;
  taskId: number;

  constructor(
    private store: Store<TasksState>,
    private route: ActivatedRoute
  ) {
    this.store
      .select(TasksReducers.isLoading)
      .subscribe((loading: boolean) => {
        this.loading = loading;
      });

    this.route.params.subscribe(res => {
      this.store.dispatch(new TasksActions.SetCurrentTask(res.id));
      this.taskId = res.id;
    });

    this.store
      .select(TasksReducers.getCurrentTask)
      .subscribe((task: Task) => {
        this.task = task;
      });
  }

  ngOnInit() {
    this.store.dispatch(new TasksActions.GetTask(this.taskId));
  }
}
